defmodule MatrixAppService.Repo.Migrations.MigrateItself do
  use Ecto.Migration

  def change do
    MatrixAppService.Migrations.migrate()
  end
end
