# MatrixAppService

> Library that adds the Matrix Application Service API to Phoenix applications.

[![[Hex.pm]](https://img.shields.io/hexpm/v/matrix_app_service?color=714a94)](https://hex.pm/packages/matrix_app_service/0.1.0)

## Installation

Add this library to your dependencies in `mix.exs`

```elixir
defp deps do
  [...]
  {:matrix_app_service, "~> 0.3.0"}
end
```

## Usage

See [documentation](https://hexdocs.pm/matrix_app_service).

## Roadmap

* [x] Authorization
* [x] Router
* [x] User query
* [x] Room query
* [x] Transaction push
* [x] Allow usage of own Phoenix endpoint
* [ ] Third party indications
* [ ] Bridge functionalities: relations between local and remote rooms
* [ ] Bridge functionalities: handling of remote sessions
* [ ] Conveniences for building bots
