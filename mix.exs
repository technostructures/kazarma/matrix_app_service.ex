defmodule MatrixAppService.MixProject do
  use Mix.Project

  def project do
    [
      name: "MatrixAppService",
      app: :matrix_app_service,
      source_url: "https://gitlab.com/technostructures/kazarma/matrix_app_service.ex",
      homepage_url: "https://gitlab.com/technostructures/kazarma/matrix_app_service.ex",
      version: "0.3.1",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      aliases: aliases(),
      deps: deps(),
      docs: [main: "MatrixAppService"],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      # elixirc_options: [warnings_as_errors: true],
      dialyzer: [
        plt_add_apps: [:ex_unit],
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"},
        ignore_warnings: ".dialyzer_ignore.exs"
      ]
    ]
  end

  defp description() do
    "Build Matrix Application Services (bridges, bots...) in Elixir."
  end

  defp package() do
    [
      licenses: ["LGPL-3.0"],
      links: %{"GitLab" => "https://gitlab.com/technostructures/kazarma/matrix_app_service.ex"}
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {MatrixAppService.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.7.0"},
      {:phoenix_view, "~> 2.0"},
      {:phoenix_ecto, "~> 4.4"},
      {:ecto, "~> 3.11.2"},
      {:ecto_sql, "~> 3.11.2"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:jason, "~> 1.3"},
      {:plug_cowboy, "~> 2.7.0"},
      {:cowboy_telemetry, "~> 0.4.0"},
      {:polyjuice_client, "~> 0.4.4"},
      # {:hackney, "~> 1.18.0", override: true},
      {:ssl_verify_fun, "~> 1.1.7", override: true},
      # {:polyjuice_client, "~> 0.3.1", path: "../polyjuice_client"},
      # {:polyjuice_client,
      #  git: "https://gitlab.com/technostructures/kazarma/polyjuice_client.git", branch: "kazarma"},
      {:ex_doc, "~> 0.34.0", only: :dev, runtime: false},
      {:junit_formatter, "~> 3.3", only: :test},
      {:postgrex, ">= 0.15.10", only: :test},
      {:credo, "~> 1.7.7", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.18", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get"],
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"]
    ]
  end
end
