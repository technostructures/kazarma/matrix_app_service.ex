defmodule MatrixAppServiceWeb.Routes do
  @moduledoc """
  Provides the Matrix Application Service API routes.

  https://matrix.org/docs/spec/application_service/r0.1.2
  """

  @doc """

  """
  defmacro __using__(_env) do
    quote do
      require unquote(__MODULE__)

      @matrix_app_services 0
    end
  end

  @doc """
  This macro injects the API routes in a Phoenix router.
  """
  # defmacro routes(opts \\ [])
  defmacro routes(:no_config) do
    quote do
      pipeline :matrix_app_service do
        plug :accepts, ["json"]
      end

      scope "/", MatrixAppServiceWeb.V1, as: :matrix do
        pipe_through :matrix_app_service

        put "/_matrix/app/v1/transactions/:txn_id", TransactionController, :push

        get "/_matrix/app/v1/users/:user_id", UserController, :query
        get "/_matrix/app/v1/rooms/:room_alias", RoomController, :query

        get "/_matrix/app/unstable/thirdparty/protocol/:protocol",
            ThirdPartyController,
            :query_protocol

        get "/_matrix/app/unstable/thirdparty/user/:protocol", ThirdPartyController, :query_users

        get "/_matrix/app/unstable/thirdparty/location/:protocol",
            ThirdPartyController,
            :query_locations

        get "/_matrix/app/unstable/thirdparty/location",
            ThirdPartyController,
            :query_location_by_alias

        get "/_matrix/app/unstable/thirdparty/user", ThirdPartyController, :query_user_by_id
      end
    end
  end

  defmacro routes(opts) do
    quote bind_quoted: [opts: opts] do
      path = Keyword.get(opts, :path, "/")
      namespace = Keyword.get(opts, :namespace, :matrix)
      homeserver_token = Keyword.fetch!(opts, :homeserver_token)
      pipeline_name = String.to_atom("matrix_api_#{@matrix_app_services}")

      pipeline pipeline_name do
        plug :accepts, ["json"]
        plug MatrixAppServiceWeb.SetConfigPlug, opts
        plug MatrixAppServiceWeb.AuthPlug, homeserver_token
      end

      scope path, MatrixAppServiceWeb.V1, as: namespace do
        pipe_through pipeline_name

        put "/_matrix/app/v1/transactions/:txn_id", TransactionController, :push

        get "/_matrix/app/v1/users/:user_id", UserController, :query
        get "/_matrix/app/v1/rooms/:room_alias", RoomController, :query

        get "/_matrix/app/unstable/thirdparty/protocol/:protocol",
            ThirdPartyController,
            :query_protocol

        get "/_matrix/app/unstable/thirdparty/user/:protocol", ThirdPartyController, :query_users

        get "/_matrix/app/unstable/thirdparty/location/:protocol",
            ThirdPartyController,
            :query_locations

        get "/_matrix/app/unstable/thirdparty/location",
            ThirdPartyController,
            :query_location_by_alias

        get "/_matrix/app/unstable/thirdparty/user", ThirdPartyController, :query_user_by_id
      end

      @matrix_app_services @matrix_app_services + 1
    end
  end
end
