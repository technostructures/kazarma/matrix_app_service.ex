defmodule MatrixAppServiceWeb.Router do
  use Phoenix.Router
  use MatrixAppServiceWeb.Routes

  # import Plug.Conn
  import Phoenix.Controller

  # if MatrixAppService.Application.start_endpoint?() do
  MatrixAppServiceWeb.Routes.routes(:no_config)
  # end
end
