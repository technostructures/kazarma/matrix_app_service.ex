defmodule MatrixAppServiceWeb.ErrorView do
  @moduledoc """
  JSON views for errors. 
  """
  use MatrixAppServiceWeb, :view

  @doc false
  def render("401.json", _assigns) do
    %{errcode: "EX.MAP.UNAUTHORIZED"}
  end

  def render("403.json", _assigns) do
    %{errcode: "EX.MAP.FORBIDDEN"}
  end

  def render("404.json", _assigns) do
    %{errcode: "EX.MAP.NOT_FOUND"}
  end

  def render("500.json", _assigns) do
    %{errcode: "EX.MAP.UNKNOWN"}
  end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.json" becomes
  # "Not Found".
  @doc false
  def template_not_found(template, _assigns) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end
end
