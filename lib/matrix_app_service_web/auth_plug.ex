defmodule MatrixAppServiceWeb.AuthPlug do
  @moduledoc """
  Implements the Application Service authorization as a Plug.

  https://spec.matrix.org/v1.8/application-service-api/#authorization
  """

  @behaviour Plug
  import Plug.Conn
  require Logger

  @doc false
  @impl Plug
  def init(homeserver_token) do
    homeserver_token
  end

  @doc false
  @impl Plug
  def call(conn, homeserver_token) when is_binary(homeserver_token) do
    verify_access_token(conn, homeserver_token)
  end

  def call(conn, homeserver_token) when is_function(homeserver_token, 0) do
    verify_access_token(conn, homeserver_token.())
  end

  def call(conn, :config) do
    verify_access_token(
      conn,
      Application.fetch_env!(:matrix_app_service, :app_service)[:homeserver_token]
    )
  end

  defp verify_access_token(conn, homeserver_token) do
    case get_req_header(conn, "authorization") do
      ["Bearer " <> ^homeserver_token] ->
        conn

      ["Bearer " <> _] ->
        Logger.warning("Received invalid homeserver token")

        respond_error(conn, 403)

      _ ->
        Logger.warning("No homeserver token provided")

        respond_error(conn, 401)
    end
  end

  defp respond_error(conn, error_code) do
    conn
    |> put_status(error_code)
    |> Phoenix.Controller.put_view(MatrixAppServiceWeb.ErrorView)
    |> Phoenix.Controller.render("#{error_code}.json")
    |> halt
  end
end
