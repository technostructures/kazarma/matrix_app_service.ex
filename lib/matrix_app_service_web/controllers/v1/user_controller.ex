defmodule MatrixAppServiceWeb.V1.UserController do
  @moduledoc """
  Controller for users.
  """
  use MatrixAppServiceWeb, :controller

  @doc """
  https://matrix.org/docs/spec/application_service/r0.1.2#get-matrix-app-v1-users-userid
  """
  def query(conn, %{"user_id" => user_id}) do
    adapter =
      conn.private[:user_adapter] ||
        MatrixAppServiceWeb.Endpoint.config(:user_adapter) ||
        raise "MatrixAppService: config key user_adapter missing"

    case adapter.query_user(user_id) do
      :ok ->
        conn
        |> put_status(200)
        |> json("{}")

      _ ->
        conn
        |> put_status(404)
        |> json("")
    end
  end
end
