defmodule MatrixAppServiceWeb.V1.TransactionController do
  @moduledoc """
  Controller for transactions.
  """
  use MatrixAppServiceWeb, :controller
  alias MatrixAppService.Event
  require Logger

  @doc """
  https://matrix.org/docs/spec/application_service/r0.1.2#put-matrix-app-v1-transactions-txnid
  """
  def push(conn, %{"events" => events}) do
    adapter =
      conn.private[:transaction_adapter] ||
        MatrixAppServiceWeb.Endpoint.config(:transaction_adapter) ||
        raise "MatrixAppService: missing config key: room_adapter"

    # Enum.each(events, &create_event(&1, adapter))
    Enum.each(events, fn event_params ->
      case Event.new(event_params) do
        {:ok, event} ->
          adapter.new_event(event)

        error ->
          Logger.error(inspect(error))
      end
    end)

    send_resp(conn, 200, "{}")
  rescue
    # for development, we may prefere acknowledging transactions even if processing them fails
    err ->
      if Application.get_env(:matrix_app_service, :ignore_exceptions, false) do
        Logger.error(Exception.format(:error, err, __STACKTRACE__))
        send_resp(conn, 200, "{}")
      else
        reraise err, __STACKTRACE__
      end
  end
end
