defmodule MatrixAppServiceWeb.V1.RoomController do
  @moduledoc """
  Controller for rooms.
  """
  use MatrixAppServiceWeb, :controller

  @doc """
  https://matrix.org/docs/spec/application_service/r0.1.2#get-matrix-app-v1-rooms-roomalias
  """
  def query(conn, %{"room_alias" => room_alias}) do
    adapter =
      conn.private[:room_adapter] ||
        MatrixAppServiceWeb.Endpoint.config(:room_adapter) ||
        raise "MatrixAppService: config key room_adapter missing"

    case adapter.query_alias(room_alias) do
      :ok ->
        conn
        |> put_status(200)
        |> json("{}")

      _ ->
        conn
        |> put_status(404)
        |> json("")
    end
  end
end
