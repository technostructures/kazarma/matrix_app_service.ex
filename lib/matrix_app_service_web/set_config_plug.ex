defmodule MatrixAppServiceWeb.SetConfigPlug do
  @moduledoc """
  This plug allows to set compile time configuration as private conn parameters.
  """

  @behaviour Plug
  import Plug.Conn
  require Logger

  @doc false
  @impl Plug
  def init(opts) do
    opts
  end

  @doc false
  @impl Plug
  def call(conn, opts) do
    conn
    |> put_config(:transaction_adapter, Keyword.fetch!(opts, :transaction_adapter))
    |> put_config(:room_adapter, Keyword.fetch!(opts, :room_adapter))
    |> put_config(:user_adapter, Keyword.fetch!(opts, :user_adapter))
    |> put_config(:homeserver_token, Keyword.fetch!(opts, :homeserver_token))
    |> put_config(:access_token, Keyword.fetch!(opts, :access_token))
    |> put_config(:base_url, Keyword.fetch!(opts, :base_url))
    |> put_config(:path, Keyword.fetch!(opts, :path))
  end

  defp put_config(conn, key, :config) do
    value = Application.fetch_env!(:matrix_app_service, :app_service)[:key]
    put_private(conn, key, value)
  end

  defp put_config(conn, key, value) do
    put_private(conn, key, value)
  end
end
