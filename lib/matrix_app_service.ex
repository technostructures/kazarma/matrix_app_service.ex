defmodule MatrixAppService do
  @moduledoc """
  Library that adds Matrix Application Service functionalities to Phoenix
  applications.

  ## Installation

  Add this library to your dependencies in `mix.exs`

  ```
  defp deps do
    # ...,
    {:matrix_app_service, "~> 0.1.0"}
  end
  ```

  ## Usage

  ### Write adapters

  Create one or multiple modules that implement the following modules:
  `MatrixAppService.Adapter.Room`, `MatrixAppService.Adapter.User`,
  `MatrixAppService.Adapter.Transaction`.

  For instance:

  ```
  @behaviour MatrixAppService.Adapter.Room

  @impl MatrixAppService.Adapter.Room
  def query_alias(room_alias) do
    # Do something with the room alias
    # If the room exists, return :ok
  end

  @behaviour MatrixAppService.Adapter.User

  @impl MatrixAppService.Adapter.User
  def query_user(user_id) do
    # Do something with the user ID
    # If the user exists, return :ok
  end

  @behaviour MatrixAppService.Adapter.Transaction

  @impl MatrixAppService.Adapter.Transaction
  def new_event(%MatrixAppService.Event{type: type, content: content}) do
    # Do something with the event
  end
  ```

  ### Configure

  #### Option 1: Using the :matrix_app_service supervision tree

  Configuration:

  ```
  config :matrix_app_service,
    internal_supervisor: true,
    transaction_adapter: App.Matrix.Transaction,
    room_adapter: App.Matrix.Room,
    user_adapter: App.Matrix.User,
    path: "/matrix"
    base_url: "http://synapse:8008",
    access_token: "access token",
    homeserver_token: "homeserver token"
  ```

  #### Option 2: Using the :matrix_app_service endpoint in your own supervision tree

  In your application module:

  ```
    children = [
      # ...,
      {MatrixAppServiceWeb.Endpoint, app_service_config()}
    ]

  # ...

  defp app_service_config(), do: Application.get_env(:app, :app_service)
  ```

  Configuration:

  ```
  config :app, :app_service,
    transaction_adapter: App.Matrix.Transaction,
    room_adapter: App.Matrix.Room,
    user_adapter: App.Matrix.User,
    path: "/matrix"
    base_url: "http://synapse:8008",
    access_token: "access token",
    homeserver_token: "homeserver token"
  ```

  #### Option 3: Using your own endpoint

  In your Phoenix Router:

  ```
  use MatrixAppServiceWeb.Routes

  MatrixAppServiceWeb.Routes.routes(Application.get_env(:app, :app_service))
  ```

  Configuration:

  ```
  config :app, :app_service,
    transaction_adapter: App.Matrix.Transaction,
    room_adapter: App.Matrix.Room,
    user_adapter: App.Matrix.User,
    path: "/matrix"
    base_url: "http://synapse:8008",
    access_token: "access token",
    homeserver_token: "homeserver token"
  ```

  ### Use the Client-Server API

  You can use the functions in `MatrixAppService.Client`
  """
end
