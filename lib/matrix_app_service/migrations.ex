defmodule MatrixAppService.Migrations do
  @moduledoc """
  Module containing migrations for tables used in bridge mode. Should be used like this:

  TODO: example
  """

  use Ecto.Migration

  def migrate do
    create_users()
    create_rooms()
    create_events()
    remove_indexes_uniqueness()
  end

  def create_users do
    create table("users") do
      add(:local_id, :string)
      add(:remote_id, :string)
      add(:data, :map)

      timestamps()
    end

    create(unique_index(:users, [:local_id]))
    create(unique_index(:users, [:remote_id]))
  end

  def create_rooms do
    create table("rooms") do
      add(:local_id, :string)
      add(:remote_id, :string)
      add(:data, :map)

      timestamps()
    end

    create(unique_index(:rooms, [:local_id]))
    create(unique_index(:rooms, [:remote_id]))
  end

  def create_events do
    create table("events") do
      add(:local_id, :string)
      add(:remote_id, :string)
      add(:room_id, :string)
      add(:sender_id, :string)

      timestamps()
    end

    create(unique_index(:events, [:local_id]))
    create(unique_index(:events, [:remote_id]))
  end

  def remove_indexes_uniqueness do
    drop(index(:users, [:local_id]))
    drop(index(:users, [:remote_id]))
    create(index(:users, [:local_id]))
    create(index(:users, [:remote_id]))
    create(unique_index(:users, [:local_id, :remote_id]))

    drop(index(:rooms, [:local_id]))
    drop(index(:rooms, [:remote_id]))
    create(index(:rooms, [:local_id]))
    create(index(:rooms, [:remote_id]))
    create(unique_index(:rooms, [:local_id, :remote_id]))

    drop(index(:events, [:local_id]))
    drop(index(:events, [:remote_id]))
    create(index(:events, [:local_id]))
    create(index(:events, [:remote_id]))
    create(unique_index(:events, [:local_id, :remote_id]))
  end

  @deprecated "Use both create_users/1 and create_rooms/1 instead"
  def change do
    create_users()
    create_rooms()
  end

  @deprecated "Use create_events/1 instead"
  def create_events_table do
    create_events()
  end
end
