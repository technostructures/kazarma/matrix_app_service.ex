defmodule MatrixAppService.Client do
  @moduledoc """
  Convenience wrapper around `Polyjuice.Client.LowLevel`.

  Library users can use the wrapped functions or call `user/1` and pass the
  returned struct to Polyjuice functions.
  """
  @behaviour MatrixAppService.ClientBehaviour

  @type create_options :: [
          access_token: String.t() | nil,
          user_id: String.t() | nil,
          device_id: String.t() | nil,
          storage: Polyjuice.Client.Storage.t() | nil
        ]
  @type client_options :: {:base_url, String.t()} | create_options()

  @doc """
  Return a client for the application service.

  By default, the client gets its homeserver URL and access token from configuration.
  Different options can be provided to override the defaults, those are:

  * `:base_url`: homerver URL
  * `:acces_token`: access token
  * `:device_id`: device ID
  * `:user_id`: user ID
  * `:storage`: a `t:Polyjuice.Client.Storage.t/0`
  """
  @impl true
  def client(options \\ []) do
    base_url =
      Keyword.get(options, :base_url) ||
        (MatrixAppService.Application.start_endpoint?() &&
           MatrixAppServiceWeb.Endpoint.config(:base_url)) ||
        Application.get_env(:matrix_app_service, :app_service)[:base_url] ||
        raise "MatrixAppService: config key base_url missing"

    access_token =
      Keyword.get(options, :access_token) ||
        (MatrixAppService.Application.start_endpoint?() &&
           MatrixAppServiceWeb.Endpoint.config(:access_token)) ||
        Application.get_env(:matrix_app_service, :app_service)[:access_token] ||
        raise "MatrixAppService: config key access_token missing"

    default_options = [
      access_token: access_token,
      device_id: "APP_SERVICE",
      application_service: true
    ]

    options =
      default_options
      |> Keyword.merge(options)
      |> Keyword.merge(Application.get_env(:matrix_app_service, :client_options, []))

    Polyjuice.Client.LowLevel.new(base_url, options)
  end

  @doc """
  Create a Matrix room.

  Arguments:
  1. `options`: see `Polyjuice.Client.Room.create_room/2`
  2. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def create_room(options \\ [], client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.create_room(options)
  end

  @doc """
  Create a new alias for a Matrix room.

  Arguments:
  1. `room_id`: room ID
  2. `room_alias`: room alias
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def create_alias(room_id, room_alias, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.create_alias(room_id, room_alias)
  end

  @doc """
  Send a message to a Matrix room.

  Arguments:
  1. `room_id`: room ID
  2. `msg`: see `Polyjuice.Client.Room.send_message/3`
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def send_message(room_id, msg, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.send_message(room_id, msg)
  end

  @doc """
  Register a new Matrix user.

  Arguments:
  1. `options`: a keyword list that can contain these keys:
    * `:inhibit_login`: true
    * `:device_id`: device ID, defaults to `"APP_SERVICE"`
    * `:initial_device_display_name`: device name, defaults to
      `"ApplicationService"`
    * `:kind`: kind of account to register, defaults to `"user"`, can also be
      `"guest"`
  2. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def register(options \\ [], client_options \\ []) do
    default_options = [
      inhibit_login: true,
      device_id: "APP_SERVICE",
      initial_device_display_name: "Application Service"
    ]

    options = Keyword.merge(default_options, options)

    client(client_options)
    |> Polyjuice.Client.LowLevel.register(options)
  end

  @impl true
  def create_attachment_message(data, options, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Attachment.new(data, options)
  end

  @doc """
  Search a Matrix user in the User Directory.

  Arguments:
  1. `search_term`: the search term for the user
  2. `limit`: a search limit number, defaults to `false` (unlimited)
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def search_user(search_term, limit \\ nil, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.UserDirectory.search_user(search_term, limit)
  end

  @doc """
  Get room ID and additional information, given a room's alias

  Arguments:
  1. `room_alias`: the alias to look for
  2. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def get_alias(room_alias, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.get_alias(room_alias)
  end

  @doc """
  Get user profile.

  Arguments:
  1. `user_id`: the ID of the user whose profile should be retrieved. If ommited, it defaults to the user_id represented by the client
  2. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def get_profile(user_id \\ nil, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Profile.get_profile(user_id)
  end

  @doc """
  Modify avatar url for the user.

  Arguments:
  1. `user_id`: the ID of the user whose profile should be retrieved. If ommited, it defaults to the user_id represented by the client
  2. `avatar_url`: an `mxc:` URL specifying the location of the new profile
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def put_avatar_url(user_id \\ nil, avatar_url, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Profile.put_avatar_url(user_id, avatar_url)
  end

  @doc """
  Modify display name for the user.

  Arguments:
  1. `user_id`: the ID of the user whose profile should be retrieved.  If ommited, it defaults to the user_id represented by the client
  2. `displayname`: the new displayname
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def put_displayname(user_id, displayname, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Profile.put_displayname(user_id, displayname)
  end

  @doc """
  Get the data on an account.

  Arguments:
  1. `user_id`: the ID of the user whose profile should be retrieved.  If ommited, it defaults to the user_id represented by the client
  2. `event_type`: the type of event queried
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def get_data(user_id, event_type, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Account.get_data(user_id, event_type)
  end

  @doc """
  Put the data on an account.

  Arguments:
  1. `user_id`: the ID of the user whose profile should be retrieved.  If ommited, it defaults to the user_id represented by the client
  2. `event_type`: the type of event queried
  3. `data`: the new data
  4. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def put_data(user_id, event_type, data, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Account.put_data(user_id, event_type, data)
  end

  @doc """
  Join a room.

  Arguments:
  1. `room_id`: the room ID
  2. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def join(room_id, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.join(room_id)
  end

  @doc """
  Upload a file to the media repository.

  Arguments:
  1. `data`: `Polyjuice.Client.Media.upload/3`
  2. `options`: `Polyjuice.Client.Media.upload/3`
  3. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def upload(data, options, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Media.upload(data, options)
  end

  @doc """
  Send a redact event to a room.

  Arguments:
  1. `room_id`: the ID of the user whose profile should be retrieved.  If ommited, it defaults to the user_id represented by the client.
  2. `event`: the redact event
  3. `reason`: the reason _(optional)_
  4. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def redact_message(room_id, event, reason \\ nil, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.redact_message(room_id, event, reason)
  end

  @doc """
  Get a state event from a room.

  Arguments:
  1. `room_id`: the ID of the user whose profile should be retrieved.  If ommited, it defaults to the user_id represented by the client.
  2. `event_type`: the type of event
  3. `state_key`: the event state key
  4. `client_options`: see `client/1` _(optional)_

  If `event_type` is not provided, returns a list of events.

  If `event_type` and `state_key` are provided, returns an event content.
  `state_key` can be omitted but this will return events that have a blank state
  key, not events that have "any state key".
  """
  @impl true
  def get_state(room_id, event_type \\ nil, state_key \\ "", client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.get_state(room_id, event_type, state_key)
  end

  @doc """
  Send a state event to a room.

  Arguments:
  1. `room_id`: the ID of the user whose profile should be retrieved.  If ommited, it defaults to the user_id represented by the client.
  2. `event_type`: the type of event
  3. `state_key`: the event state key
  4. `event_content`: the event content
  5. `client_options`: see `client/1` _(optional)_
  """
  @impl true
  def send_state_event(room_id, event_type, state_key, event_content, client_options \\ []) do
    client(client_options)
    |> Polyjuice.Client.Room.send_state_event(room_id, event_type, state_key, event_content)
  end
end
