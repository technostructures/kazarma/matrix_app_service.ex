defmodule MatrixAppService.Adapter.User do
  @moduledoc """
  Behaviour for a module that handles rooms reserved by the application service.
  """

  @doc """
  Responds to a user query by Matrix ID. If the user exists, implementations
  should create the user (for instance by using
  `MatrixAppService.Client.register/2`) then return `:ok`. If the user
  doesn't exist, they should return anything else.
  """
  @callback query_user(String.t()) :: :ok | Any
end
