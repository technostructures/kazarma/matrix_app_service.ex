defmodule MatrixAppService.Adapter.Room do
  @moduledoc """
  Behaviour for a module that handles rooms reserved by the application service.
  """

  @doc """
  Responds to a room query by alias. If the room exists, implementations should
  create the room (for instance by using
  `MatrixAppService.Client.create_room/1`) then return `:ok`. If the room
  doesn't exist, they should return anything else.
  """
  @callback query_alias(String.t()) :: :ok | any
end
