defmodule MatrixAppService.Adapter.Transaction do
  @moduledoc """
  Behaviour for a module that handles transactions involving registered users
  and rooms.
  """

  @doc """
  Called when the application service receives an event.
  """
  @callback new_event(%MatrixAppService.Event{}) :: any()
end
