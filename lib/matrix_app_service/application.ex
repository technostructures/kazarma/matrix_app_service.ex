defmodule MatrixAppService.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      # MatrixAppServiceWeb.Telemetry,
      # Start the PubSub system
      # {Phoenix.PubSub, name: MatrixAppService.PubSub},
      # Start the Endpoint (http/https)
      # MatrixAppServiceWeb.Endpoint
      # Start a worker by calling: MatrixAppService.Worker.start_link(arg)
      # {MatrixAppService.Worker, arg}
    ]

    children =
      if start_endpoint?(),
        do: [
          {MatrixAppServiceWeb.Endpoint, endpoint_config()}
          | children
        ],
        else: children

    children =
      if start_repo?(),
        do: [
          MatrixAppService.Repo
          | children
        ],
        else: children

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MatrixAppService.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    if start_endpoint?() do
      MatrixAppServiceWeb.Endpoint.config_change(changed, removed)
    end

    :ok
  end

  def start_endpoint? do
    Application.get_env(:matrix_app_service, :internal_supervisor, false)
  end

  def start_repo? do
    Application.get_env(:matrix_app_service, :internal_repo, false)
  end

  def endpoint_config do
    [
      transaction_adapter: Application.fetch_env!(:matrix_app_service, :transaction_adapter),
      room_adapter: Application.fetch_env!(:matrix_app_service, :room_adapter),
      user_adapter: Application.fetch_env!(:matrix_app_service, :user_adapter),
      homeserver_token: Application.fetch_env!(:matrix_app_service, :homeserver_token),
      access_token: Application.fetch_env!(:matrix_app_service, :access_token),
      base_url: Application.fetch_env!(:matrix_app_service, :base_url),
      path: Application.get_env(:matrix_app_service, :path, "/")
    ]
  end
end
