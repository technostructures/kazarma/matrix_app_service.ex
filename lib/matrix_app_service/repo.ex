defmodule MatrixAppService.Repo do
  use Ecto.Repo,
    otp_app: :matrix_app_service,
    adapter: Ecto.Adapters.Postgres
end
