defmodule MatrixAppService.BridgeConfig do
  @moduledoc """
  The Bridge config.
  """

  defmacro __using__(repo: repo) do
    quote bind_quoted: [repo: repo] do
      @repo repo

      import Ecto.Query, warn: false
      # alias MatrixAppService.Repo

      MatrixAppService.BridgeConfig.user_functions()
      MatrixAppService.BridgeConfig.room_functions()
      MatrixAppService.BridgeConfig.event_functions()
    end
  end

  # credo:disable-for-next-line Credo.Check.Refactor.CyclomaticComplexity
  defmacro user_functions do
    quote do
      alias MatrixAppService.Bridge.User

      @doc """
      Returns the list of users.

      ## Examples

          iex> list_users()
          [%User{}, ...]

      """
      def list_users do
        @repo.all(User)
      end

      @doc """
      Gets a single user.

      Raises `Ecto.NoResultsError` if the User does not exist.

      ## Examples

          iex> get_user!(123)
          %User{}

          iex> get_user!(456)
          ** (Ecto.NoResultsError)

      """
      def get_user!(id), do: @repo.get!(User, id)

      def get_user_by_local_id(local_id),
        do: @repo.get_by(User, local_id: local_id)

      def get_user_by_remote_id(remote_id),
        do: @repo.get_by(User, remote_id: remote_id)

      @doc """
      Creates a user.

      ## Examples

          iex> create_user(%{field: value})
          {:ok, %User{}}

          iex> create_user(%{field: bad_value})
          {:error, %Ecto.Changeset{}}

      """
      def create_user(attrs \\ %{}) do
        %User{}
        |> User.changeset(attrs)
        |> @repo.insert()
      end

      def upsert_user(attrs, selectors) do
        case @repo.get_by(User, selectors) do
          %User{} = user ->
            update_user(user, attrs)

          _ ->
            create_user(Enum.into(selectors, attrs, fn {k, v} -> {to_string(k), v} end))
        end
      end

      @doc """
      Updates a user.

      ## Examples

          iex> update_user(user, %{field: new_value})
          {:ok, %User{}}

          iex> update_user(user, %{field: bad_value})
          {:error, %Ecto.Changeset{}}

      """
      def update_user(%User{} = user, attrs) do
        user
        |> User.changeset(attrs)
        |> @repo.update()
      end

      @doc """
      Deletes a user.

      ## Examples

          iex> delete_user(user)
          {:ok, %User{}}

          iex> delete_user(user)
          {:error, %Ecto.Changeset{}}

      """
      def delete_user(%User{} = user) do
        @repo.delete(user)
      end

      @doc """
      Returns an `%Ecto.Changeset{}` for tracking user changes.

      ## Examples

          iex> change_user(user)
          %Ecto.Changeset{data: %User{}}

      """
      def change_user(%User{} = user, attrs \\ %{}) do
        User.changeset(user, attrs)
      end
    end
  end

  defmacro room_functions do
    quote do
      alias MatrixAppService.Bridge.Room

      @doc """
      Returns the list of rooms.

      ## Examples

          iex> list_rooms()
          [%Room{}, ...]

      """
      def list_rooms do
        @repo.all(Room)
      end

      @doc """
      Gets a single room.

      Raises `Ecto.NoResultsError` if the Room does not exist.

      ## Examples

          iex> get_room!(123)
          %Room{}

          iex> get_room!(456)
          ** (Ecto.NoResultsError)

      """
      def get_room!(id), do: @repo.get!(Room, id)

      def get_room_by_local_id(local_id),
        do: @repo.get_by(Room, local_id: local_id)

      def get_room_by_remote_id(remote_id),
        do: @repo.get_by(Room, remote_id: remote_id)

      @doc """
      Creates a room.

      ## Examples

          iex> create_room(%{field: value})
          {:ok, %Room{}}

          iex> create_room(%{field: bad_value})
          {:error, %Ecto.Changeset{}}

      """
      def create_room(attrs \\ %{}) do
        %Room{}
        |> Room.changeset(attrs)
        |> @repo.insert()
      end

      @doc """
      Updates a room.

      ## Examples

          iex> update_room(room, %{field: new_value})
          {:ok, %Room{}}

          iex> update_room(room, %{field: bad_value})
          {:error, %Ecto.Changeset{}}

      """
      def update_room(%Room{} = room, attrs) do
        room
        |> Room.changeset(attrs)
        |> @repo.update()
      end

      @doc """
      Deletes a room.

      ## Examples

          iex> delete_room(room)
          {:ok, %Room{}}

          iex> delete_room(room)
          {:error, %Ecto.Changeset{}}

      """
      def delete_room(%Room{} = room) do
        @repo.delete(room)
      end

      @doc """
      Returns an `%Ecto.Changeset{}` for tracking room changes.

      ## Examples

          iex> change_room(room)
          %Ecto.Changeset{data: %Room{}}

      """
      def change_room(%Room{} = room, attrs \\ %{}) do
        Room.changeset(room, attrs)
      end
    end
  end

  # credo:disable-for-next-line Credo.Check.Refactor.CyclomaticComplexity
  defmacro event_functions do
    quote do
      alias MatrixAppService.Bridge.Event

      @doc """
      Returns the list of events.

      ## Examples

          iex> list_events()
          [%Event{}, ...]

      """
      def list_events do
        @repo.all(Event)
      end

      @doc """
      Gets a single event.

      Raises `Ecto.NoResultsError` if the Event does not exist.

      ## Examples

          iex> get_event!(123)
          %Event{}

          iex> get_event!(456)
          ** (Ecto.NoResultsError)

      """
      def get_event!(id), do: @repo.get!(Event, id)

      def get_event_by_local_id(local_id),
        do: @repo.get_by(Event, local_id: local_id)

      def get_event_by_remote_id(remote_id),
        do: @repo.get_by(Event, remote_id: remote_id)

      def get_events_by_local_id(local_id),
        do: @repo.all(from(Event, where: [local_id: ^local_id]))

      def get_events_by_remote_id(remote_id),
        do: @repo.all(from(Event, where: [remote_id: ^remote_id]))

      def get_last_event_in_room(room_id) do
        from(Event,
          where: [room_id: ^room_id],
          order_by: [desc: :inserted_at],
          limit: 1
        )
        |> @repo.one()
      end

      @doc """
      Creates an event.

      ## Examples

          iex> create_event(%{field: value})
          {:ok, %Event{}}

          iex> create_event(%{field: bad_value})
          {:error, %Ecto.Changeset{}}

      """
      def create_event(attrs \\ %{}) do
        %Event{}
        |> Event.changeset(attrs)
        |> @repo.insert()
      end

      @doc """
      Updates an event.

      ## Examples

          iex> update_event(event, %{field: new_value})
          {:ok, %Event{}}

          iex> update_event(event, %{field: bad_value})
          {:error, %Ecto.Changeset{}}

      """
      def update_event(%Event{} = event, attrs) do
        event
        |> Event.changeset(attrs)
        |> @repo.update()
      end

      @doc """
      Deletes an event.

      ## Examples

          iex> delete_event(event)
          {:ok, %Event{}}

          iex> delete_event(event)
          {:error, %Ecto.Changeset{}}

      """
      def delete_event(%Event{} = event) do
        @repo.delete(event)
      end

      @doc """
      Returns an `%Ecto.Changeset{}` for tracking event changes.

      ## Examples

          iex> change_event(event)
          %Ecto.Changeset{data: %Event{}}

      """
      def change_event(%Event{} = event, attrs \\ %{}) do
        Event.changeset(event, attrs)
      end
    end
  end
end
