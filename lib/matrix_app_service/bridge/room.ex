defmodule MatrixAppService.Bridge.Room do
  @moduledoc """
  Schema and helpers for keeping track of room correspondence. 
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "rooms" do
    field(:data, :map)
    field(:local_id, :string)
    field(:remote_id, :string)

    timestamps()
  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:local_id, :remote_id, :data])
    # |> validate_required([:local_id, :remote_id, :data])
    |> unique_constraint([:local_id, :remote_id])
  end
end
