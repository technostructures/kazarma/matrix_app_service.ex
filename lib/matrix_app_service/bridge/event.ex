defmodule MatrixAppService.Bridge.Event do
  @moduledoc """
  Schema and helpers for keeping track of events sent through the bridge.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "events" do
    field(:local_id, :string)
    field(:remote_id, :string)
    field(:room_id, :string)
    field(:sender_id, :string)

    timestamps()
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, [:local_id, :remote_id, :room_id, :sender_id])
    # |> validate_required([:local_id, :remote_id])
    |> unique_constraint([:local_id, :remote_id])
  end
end
