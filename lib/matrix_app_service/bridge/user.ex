defmodule MatrixAppService.Bridge.User do
  @moduledoc """
  Schema and helpers for keeping track of user correspondence. 
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:data, :map)
    field(:local_id, :string)
    field(:remote_id, :string)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:local_id, :remote_id, :data])
    # |> validate_required([:local_id, :remote_id, :data])
    |> unique_constraint([:local_id, :remote_id])
  end
end
