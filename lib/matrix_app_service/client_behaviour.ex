defmodule MatrixAppService.ClientBehaviour do
  @moduledoc """
  Behaviour defining callbacks implemented in `MatrixAppService.Client`. Can be used to mock the client, for instance with `mox`:

  TODO: example
  """

  @type create_options :: [
          access_token: String.t() | nil,
          user_id: String.t() | nil,
          device_id: String.t() | nil,
          storage: Polyjuice.Client.Storage.t() | nil
        ]
  @type client_options :: {:base_url, String.t()} | create_options()
  @type data_format ::
          {:data, binary(), String.t()} | {:file, String.t()} | {:url, String.t() | URI.t()}

  @callback client() ::
              Polyjuice.Client.LowLevel.t()
  @callback client([client_options()]) ::
              Polyjuice.Client.LowLevel.t()
  @callback create_room(Keyword.t()) :: {:ok, map()} | any
  @callback create_room(Keyword.t(), client_options()) :: {:ok, map()} | any
  @callback create_alias(String.t(), String.t(), client_options()) :: {:ok, String.t()} | any
  @callback send_message(String.t(), String.t()) :: {:ok, String.t()} | any
  @callback send_message(String.t(), String.t(), client_options()) :: {:ok, String.t()} | any
  @callback register() ::
              {:ok, String.t()} | any
  @callback register(list()) ::
              {:ok, String.t()} | any
  @callback register(list(), client_options()) ::
              {:ok, String.t()} | any
  @callback get_profile(String.t()) :: {:ok, map()} | any
  @callback get_profile(String.t(), client_options()) :: {:ok, map()} | any
  @callback get_data(String.t(), String.t()) :: {:ok, map()} | any
  @callback get_data(String.t(), String.t(), client_options()) :: {:ok, map()} | any
  @callback put_data(String.t(), String.t(), map()) ::
              {:ok, map()} | any
  @callback put_data(String.t(), String.t(), map(), client_options()) ::
              {:ok, map()} | any
  @callback put_displayname(String.t(), String.t()) :: :ok | any
  @callback put_displayname(String.t(), String.t(), client_options()) :: :ok | any
  @callback put_avatar_url(String.t(), String.t()) :: :ok | any
  @callback put_avatar_url(String.t(), String.t(), client_options()) :: :ok | any
  @callback join(String.t()) :: :ok | any
  @callback join(String.t(), client_options()) :: :ok | any
  @callback upload(binary, Keyword.t()) :: {:ok, String.t()} | any
  @callback upload(binary, Keyword.t(), client_options()) :: {:ok, String.t()} | any
  @callback create_attachment_message(data_format(), Keyword.t(), client_options()) ::
              Polyjuice.Client.Attachment.attachment()
  @callback redact_message(String.t(), String.t(), String.t()) ::
              {:ok, String.t()} | any
  @callback redact_message(String.t(), String.t(), String.t(), client_options()) ::
              {:ok, String.t()} | any
  @callback search_user(String.t(), integer()) ::
              {:ok, String.t()} | any
  @callback search_user(String.t(), integer(), client_options()) ::
              {:ok, String.t()} | any
  @callback get_alias(String.t()) ::
              {:ok, String.t()} | any
  @callback get_alias(String.t(), client_options()) ::
              {:ok, String.t()} | any
  @callback get_state(String.t(), String.t() | nil, String.t()) ::
              {:ok, String.t()} | any
  @callback get_state(String.t(), String.t() | nil, String.t(), client_options()) ::
              {:ok, String.t()} | any
  @callback send_state_event(String.t(), String.t(), String.t(), Polyjuice.Util.event_content()) ::
              {:ok, String.t()} | any
  @callback send_state_event(
              String.t(),
              String.t(),
              String.t(),
              Polyjuice.Util.event_content(),
              client_options()
            ) ::
              {:ok, String.t()} | any
end
