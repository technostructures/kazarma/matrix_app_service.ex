defmodule MatrixAppService.Bridge do
  @moduledoc """
  This module is used when the library uses its own repo. 
  """

  use MatrixAppService.BridgeConfig, repo: MatrixAppService.Repo
end
