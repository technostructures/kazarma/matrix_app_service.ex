# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2022-08-11

### Added

- Library has an API allowing to keep track of users, rooms and events, using Ecto, if needed
- Library has more helpers in MatrixAppService.Client
- Updated dependencies (Polyjuice: v0.4.4)

## [0.2.0] - 2020-12-01

### Added

- Library can be used without a Phoenix Endpoint or Router

## [0.1.0] - 2020-10-24

### Added

- Library can inject Phoenix Router routes
- Checks for valid homeserver token
- Responds to queries for users
- Responds to queries for aliases
- Responds to pushed transactions
