import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :matrix_app_service, MatrixAppService.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: "matrix_app_service_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :matrix_app_service, MatrixAppServiceWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warning

config :matrix_app_service,
  internal_supervisor: true,
  internal_repo: true,
  transaction_adapter: MatrixAppService.TestTransactionAdapter,
  room_adapter: MatrixAppService.TestRoomAdapter,
  user_adapter: MatrixAppService.TestUserAdapter,
  homeserver_token: "homeserver token",
  access_token: "homeserver token",
  base_url: "http://homeserver"
