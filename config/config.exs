# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :matrix_app_service,
  ecto_repos: [MatrixAppService.Repo]

# Configures the endpoint
config :matrix_app_service, MatrixAppServiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "o53gWptgO8ItBlXMZaps/r/y/iQuZPuB/YkKl7H2UfTVkaQVDqR+3LtFLUWvVUtb",
  render_errors: [view: MatrixAppServiceWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: MatrixAppService.PubSub,
  live_view: [signing_salt: "FhCM5pjr"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :matrix_app_service,
  internal_supervisor: true

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
