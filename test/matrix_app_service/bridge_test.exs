defmodule MatrixAppService.BridgeTest do
  use MatrixAppService.DataCase

  alias MatrixAppService.Bridge

  describe "users" do
    alias MatrixAppService.Bridge.User

    @valid_attrs %{data: %{}, local_id: "some local_id", remote_id: "some remote_id"}
    @update_attrs %{
      data: %{},
      local_id: "some updated local_id",
      remote_id: "some updated remote_id"
    }

    # @invalid_attrs %{data: nil, local_id: nil, remote_id: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Bridge.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Bridge.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Bridge.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Bridge.create_user(@valid_attrs)
      assert user.data == %{}
      assert user.local_id == "some local_id"
      assert user.remote_id == "some remote_id"
    end

    #     test "create_user/1 with invalid data returns error changeset" do
    #       assert {:error, %Ecto.Changeset{}} = Bridge.create_user(@invalid_attrs)
    #     end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, %User{} = user} = Bridge.update_user(user, @update_attrs)
      assert user.data == %{}
      assert user.local_id == "some updated local_id"
      assert user.remote_id == "some updated remote_id"
    end

    #     test "update_user/2 with invalid data returns error changeset" do
    #       user = user_fixture()
    #       assert {:error, %Ecto.Changeset{}} = Bridge.update_user(user, @invalid_attrs)
    #       assert user == Bridge.get_user!(user.id)
    #     end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Bridge.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Bridge.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Bridge.change_user(user)
    end
  end

  describe "rooms" do
    alias MatrixAppService.Bridge.Room

    @valid_attrs %{data: %{}, local_id: "some local_id", remote_id: "some remote_id"}
    @update_attrs %{
      data: %{},
      local_id: "some updated local_id",
      remote_id: "some updated remote_id"
    }

    # @invalid_attrs %{data: nil, local_id: nil, remote_id: nil}

    def room_fixture(attrs \\ %{}) do
      {:ok, room} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Bridge.create_room()

      room
    end

    test "list_rooms/0 returns all rooms" do
      room = room_fixture()
      assert Bridge.list_rooms() == [room]
    end

    test "get_room!/1 returns the room with given id" do
      room = room_fixture()
      assert Bridge.get_room!(room.id) == room
    end

    test "create_room/1 with valid data creates a room" do
      assert {:ok, %Room{} = room} = Bridge.create_room(@valid_attrs)
      assert room.data == %{}
      assert room.local_id == "some local_id"
      assert room.remote_id == "some remote_id"
    end

    #     test "create_room/1 with invalid data returns error changeset" do
    #       assert {:error, %Ecto.Changeset{}} = Bridge.create_room(@invalid_attrs)
    #     end

    test "update_room/2 with valid data updates the room" do
      room = room_fixture()
      assert {:ok, %Room{} = room} = Bridge.update_room(room, @update_attrs)
      assert room.data == %{}
      assert room.local_id == "some updated local_id"
      assert room.remote_id == "some updated remote_id"
    end

    #     test "update_room/2 with invalid data returns error changeset" do
    #       room = room_fixture()
    #       assert {:error, %Ecto.Changeset{}} = Bridge.update_room(room, @invalid_attrs)
    #       assert room == Bridge.get_room!(room.id)
    #     end

    test "delete_room/1 deletes the room" do
      room = room_fixture()
      assert {:ok, %Room{}} = Bridge.delete_room(room)
      assert_raise Ecto.NoResultsError, fn -> Bridge.get_room!(room.id) end
    end

    test "change_room/1 returns a room changeset" do
      room = room_fixture()
      assert %Ecto.Changeset{} = Bridge.change_room(room)
    end
  end

  describe "events" do
    alias MatrixAppService.Bridge.Event

    @valid_attrs %{data: %{}, local_id: "some local_id", remote_id: "some remote_id"}
    # @update_attrs %{
    #   data: %{},
    #   local_id: "some updated local_id",
    #   remote_id: "some updated remote_id"
    # }

    # @invalid_attrs %{data: nil, local_id: nil, remote_id: nil}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Bridge.create_event()

      event
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Bridge.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates an event" do
      assert {:ok, %Event{} = event} = Bridge.create_event(@valid_attrs)
      # assert event.data == %{}
      assert event.local_id == "some local_id"
      assert event.remote_id == "some remote_id"
    end

    #     test "create_room/1 with invalid data returns error changeset" do
    #       assert {:error, %Ecto.Changeset{}} = Bridge.create_room(@invalid_attrs)
    #     end

    test "change_event/1 returns an event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Bridge.change_event(event)
    end
  end
end
