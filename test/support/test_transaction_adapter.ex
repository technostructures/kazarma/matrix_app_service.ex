defmodule MatrixAppService.TestTransactionAdapter do
  @moduledoc false
  @behaviour MatrixAppService.Adapter.Transaction
  require Logger

  @impl MatrixAppService.Adapter.Transaction
  def new_event(%MatrixAppService.Event{}) do
    Logger.warning("got an event")

    nil
  end
end
