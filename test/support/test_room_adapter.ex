defmodule MatrixAppService.TestRoomAdapter do
  @moduledoc false
  @behaviour MatrixAppService.Adapter.Room

  @impl MatrixAppService.Adapter.Room
  def query_alias("#existing:homeserver") do
    :ok
  end

  def query_alias(_) do
    nil
  end
end
