defmodule MatrixAppService.TestUserAdapter do
  @moduledoc false
  @behaviour MatrixAppService.Adapter.User

  @impl MatrixAppService.Adapter.User
  def query_user("@existing:homeserver") do
    :ok
  end

  def query_user(_) do
    nil
  end
end
