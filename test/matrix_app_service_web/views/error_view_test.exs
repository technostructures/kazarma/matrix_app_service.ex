defmodule MatrixAppServiceWeb.ErrorViewTest do
  use MatrixAppServiceWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 401.json" do
    assert render(MatrixAppServiceWeb.ErrorView, "401.json", []) ==
             %{errcode: "EX.MAP.UNAUTHORIZED"}
  end

  test "renders 403.json" do
    assert render(MatrixAppServiceWeb.ErrorView, "403.json", []) ==
             %{errcode: "EX.MAP.FORBIDDEN"}
  end

  test "renders 404.json" do
    assert render(MatrixAppServiceWeb.ErrorView, "404.json", []) ==
             %{errcode: "EX.MAP.NOT_FOUND"}
  end

  test "renders 500.json" do
    assert render(MatrixAppServiceWeb.ErrorView, "500.json", []) ==
             %{errcode: "EX.MAP.UNKNOWN"}
  end
end
