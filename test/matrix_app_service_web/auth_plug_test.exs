defmodule MatrixAppServiceWeb.AuthPlugTest do
  use ExUnit.Case
  use Plug.Test

  import ExUnit.CaptureLog

  test "call with correct acces token returns conn unchanged" do
    conn =
      conn(:get, "/users/2") |> Plug.Conn.put_req_header("authorization", "Bearer correct token")

    assert MatrixAppServiceWeb.AuthPlug.call(conn, "correct token") == conn
  end

  test "call with incorrect access token halts with error 403" do
    conn =
      conn(:get, "/users/2")
      |> Plug.Conn.put_req_header("authorization", "Bearer incorrect token")
      |> MatrixAppServiceWeb.AuthPlug.call("correct token")

    assert Phoenix.ConnTest.response(conn, 403) =~ "FORBIDDEN"
    assert conn.halted == true
  end

  test "call with incorrect access token gets logged" do
    conn =
      conn(:get, "/users/2")
      |> Plug.Conn.put_req_header("authorization", "Bearer incorrect token")

    assert capture_log(fn -> MatrixAppServiceWeb.AuthPlug.call(conn, "correct token") end) =~
             "Received invalid homeserver token"
  end

  test "call without access token halts with error 401" do
    conn =
      conn(:get, "/users/2")
      |> MatrixAppServiceWeb.AuthPlug.call("correct token")

    assert Phoenix.ConnTest.response(conn, 401) =~ "UNAUTHORIZED"
    assert conn.halted == true
  end

  test "call without access token gets logged" do
    conn = conn(:get, "/user/3")

    assert capture_log(fn -> MatrixAppServiceWeb.AuthPlug.call(conn, "correct token") end) =~
             "No homeserver token provided"
  end
end
