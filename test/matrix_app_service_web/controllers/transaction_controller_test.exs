defmodule MatrixAppServiceWeb.TransactionControllerTest do
  use MatrixAppServiceWeb.ConnCase

  import ExUnit.CaptureLog

  describe "push" do
    @tag authenticated: true
    test "transactions are pushed", %{conn: conn} do
      event = %{
        "age" => 42,
        "content" => %{},
        "event_id" => "event_id",
        "origin_server_ts" => 42,
        "room_id" => "room_id",
        "sender" => "sender",
        "type" => "type",
        "unsigned" => %{},
        "user_id" => "user_id"
      }

      assert capture_log(fn ->
               put(
                 conn,
                 Routes.matrix_transaction_path(conn, :push, 42),
                 %{"events" => [event]}
               )
             end) =~
               "got an event"
    end
  end
end
