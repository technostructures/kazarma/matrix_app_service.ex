defmodule MatrixAppServiceWeb.UserControllerTest do
  use MatrixAppServiceWeb.ConnCase

  describe "query" do
    @tag authenticated: true
    test "querying an existing user returns 200", %{conn: conn} do
      conn = get(conn, Routes.matrix_user_path(conn, :query, "@existing:homeserver"))
      assert json_response(conn, 200) == "{}"
    end

    @tag authenticated: true
    test "querying an non-existing user returns 404", %{conn: conn} do
      conn =
        get(conn, Routes.matrix_user_path(conn, :query, "@non-existing:homeserver"))

      assert json_response(conn, 404) == ""
    end
  end
end
