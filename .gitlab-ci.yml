stages:
  - setup
  - test
  - format
  - credo
  - dialyzer
  - docs

image: elixir:1.17-otp-27-alpine

cache: &global_cache
  key: cache-test
  when: always
  policy: pull
  paths:
    - _build
    - deps

variables:
  MIX_ENV: test

.mix:
  before_script:
    - apk update
    - apk add git
    - mix local.rebar --force
    - mix local.hex --force
    - mix deps.get
    - mix compile

prepare_test:
  stage: setup
  extends: .mix
  script:
    - mix deps.get
    - mix compile
  cache:
    <<: *global_cache
    policy: pull-push

test:
  stage: test
  needs:
    - prepare_test
  services:
    - postgres:latest
  variables:
    POSTGRES_DB: matrix_app_service_test
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_HOST: postgres
    POSTGRES_HOST_AUTH_METHOD: trust
    MIX_ENV: test
  before_script:
    - apk update
    - apk add git
    - mix local.hex --force
    - mix deps.get
    - mix compile
    - mix ecto.create
    - mix ecto.migrate
  script:
    - mix coveralls.cobertura
  coverage: '/\[TOTAL\]\s+(\d+\.\d+)%/'
  artifacts:
    when: always
    paths:
      - cover
    reports:
      junit: _build/test/lib/matrix_app_service/test-junit-report.xml
      coverage_report:
        coverage_format: cobertura
        path: cover/cobertura.xml

format:
  stage: format
  extends: .mix
  needs:
    - prepare_test
  script:
    - mix format --check-formatted

credo:
  stage: credo
  extends: .mix
  needs:
    - prepare_test
  script:
    - mix credo suggest --only readability | tee credo.log
  artifacts:
    when: always
    paths:
      - "credo.log"

dialyzer:
  extends: .mix
  stage: dialyzer
  needs:
    - prepare_test
  script:
    - mix dialyzer
  cache: 
    <<: *global_cache
    policy: pull-push
    paths:
      - _build
      - deps
      - priv/plts


# Doc generation

prepare_dev:
  stage: setup
  only:
    - master
  variables:
    MIX_ENV: dev
  script:
    - mix deps.get
    - mix compile
  cache:
    <<: *global_cache
    policy: pull-push
    key: cache-dev

pages:
  stage: docs
  only:
    - master
  needs:
    - prepare_dev
  variables:
    MIX_ENV: dev
  script:
    - mix docs -o public
  cache:
    <<: *global_cache
    key: cache-dev
  artifacts:
    paths:
      - public
